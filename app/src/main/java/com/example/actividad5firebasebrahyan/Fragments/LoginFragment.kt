package com.example.actividad5firebasebrahyan.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.actividad5firebasebrahyan.ActividadPrincipal
import com.example.actividad5firebasebrahyan.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LoginFragment : Fragment, View.OnClickListener {

    private var TAG:String ="LoginFragment"

    var btnAccederLogin: Button?=null
    var btnVolverLogin: Button?=null
    var etPasswordLogin: EditText?=null
    var etEmailLogin: EditText?=null

    private lateinit var auth: FirebaseAuth


    private lateinit var listener:View.OnClickListener
    constructor(listener : View.OnClickListener){
        this.listener=listener
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        auth = Firebase.auth

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnAccederLogin=view.findViewById(R.id.btnAccederLogin)
        btnVolverLogin=view.findViewById(R.id.btnVolverLogin)
        etPasswordLogin=view.findViewById(R.id.etPasswordLogin)
        etEmailLogin=view.findViewById(R.id.etEmailLogin)

        btnAccederLogin!!.setOnClickListener(this)
        btnVolverLogin!!.setOnClickListener(listener)


    }

     override fun onClick(v: View?) {



        val strEmail:String=etEmailLogin!!.text.toString()
        val strPassword:String=etPasswordLogin!!.text.toString()

        if(v==btnAccederLogin){

            auth.signInWithEmailAndPassword(strEmail, strPassword)
                .addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success")
                        val user = auth.currentUser
                        Toast.makeText(activity,"Autentificacion exitosa", Toast.LENGTH_LONG).show()

                        val intent: Intent = Intent(requireContext(), ActividadPrincipal :: class.java)
                        startActivity(intent)
                        requireActivity().finish()

                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.exception)
                        Toast.makeText(requireContext(), "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                    }
                }

        }

    }


}