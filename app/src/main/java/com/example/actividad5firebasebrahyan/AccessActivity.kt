package com.example.actividad5firebasebrahyan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.actividad5firebasebrahyan.Fragments.AccessFragment
import com.example.actividad5firebasebrahyan.Fragments.LoginFragment
import com.example.actividad5firebasebrahyan.Fragments.SingUpFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class AccessActivity : AppCompatActivity(),View.OnClickListener {


    private val TAG:String="Access Activity"

    private val accessFragment : AccessFragment = AccessFragment(this)
    private val loginFragment : LoginFragment = LoginFragment(this)
    private val singUpFragment : SingUpFragment = SingUpFragment(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_access)




        /*
        En esta actividad unicamenete se gestiona la aparicion del fragmento Access, a partir del cual
        se controlara el acceso a los otros dos fragmentos que forman parte de la actividad.
        para gestionar el cambio de Fragmentos se ha creado un contenedor de fragmentos, el flContainer
        */

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer,accessFragment)
            commit()
        }


    }

    override fun onClick(Boton: View?) {

        /*
        Se declaran los fragmentos de Registro y Login para poder reemplazar al Fragment Access
        en el contenedor flContainer = Frame Layot
        */



        /*-------------------------Botones del fragmento de ACCESO---------------------------------*/
        if(Boton==accessFragment.btnLoginAccess!!){

            /*---------------------------------------------------------------------------
            Se utiliza addToBackStack(null) para añadir a la pila el Fragment que se sustituye, para
            que al pulsar el botón volver desde culquiera de los dos fragmentos, se vuelva al fragmento
            de acceso, y no se cierre la apliación
             ---------------------------------------------------------------------------*/

            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flContainer,loginFragment)
                addToBackStack(null)
                commit()
            }


        }else if(Boton==accessFragment.btnSingUpAccess!!){

            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flContainer,singUpFragment)
                addToBackStack(null)
                commit()
            }
        }

        /*---------------------------------Botones del Fragmento de Login-----------------------*/

        if (Boton==loginFragment!!.btnVolverLogin){
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flContainer,accessFragment)
                commit()
            }
        }

        /*---------------------------------Botones del Fragmento de Registro-----------------------*/

        if (Boton==singUpFragment!!.btnVolverSingUp){
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flContainer,accessFragment)
                commit()
            }
        }



    }


}