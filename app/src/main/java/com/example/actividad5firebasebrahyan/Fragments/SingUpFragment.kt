package com.example.actividad5firebasebrahyan.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.actividad5firebasebrahyan.ActividadPrincipal
import com.example.actividad5firebasebrahyan.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class SingUpFragment : Fragment, View.OnClickListener {

    var etUserNameSingUp: EditText?=null
    var etEmailSingUp: EditText?=null
    var etPasswordSingUp: EditText?=null
    var etRepeatPasswordSingUp: EditText?=null
    var btnVolverSingUp: Button?=null
    var btnRegistrarmeSingUp: Button?=null

    private lateinit var auth: FirebaseAuth

    var TAG:String="SingUpFragment"

    private lateinit var listener:View.OnClickListener
    constructor(listener : View.OnClickListener){
        this.listener=listener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = Firebase.auth
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_sing_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        etUserNameSingUp=view.findViewById(R.id.etUserNameSingUp)
        etEmailSingUp=view.findViewById(R.id.etEmailSingUp)
        etPasswordSingUp=view.findViewById(R.id.etPasswordSingUp)
        etRepeatPasswordSingUp=view.findViewById(R.id.etRepeatPasswordSingUp)

        btnVolverSingUp=view.findViewById(R.id.btnVolverSingUp)
        btnRegistrarmeSingUp=view.findViewById(R.id.btnRegistrarmeSingUp)

        btnVolverSingUp!!.setOnClickListener(listener)
        btnRegistrarmeSingUp!!.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

            val strUsername:String=etUserNameSingUp!!.text.toString()
            val strEmail:String=etEmailSingUp!!.text.toString()
            val strPassword1:String=etPasswordSingUp!!.text.toString()
            val strPassword2:String=etRepeatPasswordSingUp!!.text.toString()


            if(v ==btnRegistrarmeSingUp){



                if(!usernameValidator(strUsername)){
                    Toast.makeText(activity,"El nombre de usuario debe tener al menos 5 caracteres", Toast.LENGTH_LONG).show()
                }else if(!emailValidator(strEmail)) {
                    Toast.makeText(activity, "El Email introducido no es valido", Toast.LENGTH_LONG).show()
                }else if(strPassword2!=strPassword1){
                    Toast.makeText(activity,"Las contraseñas no coinciden", Toast.LENGTH_LONG).show()
                }else if(!passwordValidator(strPassword1)){
                    Toast.makeText(activity,"Las contraseña debe contener al menos una mayuscula, una minuscula y un numero", Toast.LENGTH_LONG).show()
                }else{

                    auth.createUserWithEmailAndPassword(strEmail, strPassword1)
                        .addOnCompleteListener(requireActivity()) { task ->
                            if (task.isSuccessful) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "createUserWithEmail:success")
                                val user = auth.currentUser

                                val intent: Intent = Intent(requireContext(), ActividadPrincipal :: class.java)
                                startActivity(intent)
                                requireActivity().finish()

                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "createUserWithEmail:failure", task.exception)
                                Toast.makeText(requireContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show()

                            }

                        }

                    Toast.makeText(activity,"Registro completado con exito, revise su bandeja de entrada para validar el Email", Toast.LENGTH_LONG).show()
                }

            }

    }

    /*
    Valida que las contraseña cumpla con los requisitos de tener al menos una mayuscula y una minuscula
    8 caracteres y al menos un numero
    */
    private fun passwordValidator(Password:String):Boolean{

        var iContador:Int=0
        var bNumero:Boolean=false
        var bMayuscula:Boolean=false
        var bValida:Boolean=false
        var bMinuscula:Boolean=false

        for (i in Password){

            var caracter:String?=i.toString()

            if(caracter=="9"|| caracter=="8"|| caracter=="7"|| caracter=="6"|| caracter=="5"|| caracter=="4"|| caracter=="3"|| caracter=="2"|| caracter=="1"|| caracter=="0"){
                bNumero=true
            }

            if((caracter!=(caracter?.toLowerCase()))){
                bMayuscula=true
            }

            if((caracter!=(caracter?.toUpperCase()))){
                bMinuscula=true
            }

            iContador=iContador+1

        }

        if(bNumero && bMayuscula && bMinuscula && (iContador>=8)){

            bValida=true
        }

        return bValida
    }

    /*
     Valida que el Email sea valido, comprobando que el email tenga solo una "@" y que haya un punto
     despues de esta para comprobar que tiene dominio
     */
    private fun emailValidator(Email:String):Boolean{

        var bValido:Boolean=false
        var bArroba:Boolean=false
        var bPunto:Boolean=false
        var iCuentaArrobas:Int=0

        //Evalua que despues de la arroba haya al menos un punto, para comprobar que el correo
        //Tenga un dominio, ademas previamente comprueba que haya una 'arroba'.
        //Además comprueba que solo haya una arroba, si hay más ta,bien dará por INVALIDO el Email

        for (i in Email){
            var caracter:String?=i.toString()
            if(caracter=="@"){
                bArroba=true
                iCuentaArrobas += 1
            }

            if(iCuentaArrobas>1){
                bArroba=false
            }

            if(bArroba && (caracter==".")){
                bPunto=true
            }
        }
        if((bArroba==true)&&(bPunto==true)){
            bValido=true
        }

        return bValido
    }

    /*
     valida que el Username sea de al menos 5 caracteres
    */
    private fun usernameValidator(Username:String):Boolean{

        var iLetras:Int=0

        for (i in Username){
            iLetras += 1
        }

        return iLetras>=5
    }



}